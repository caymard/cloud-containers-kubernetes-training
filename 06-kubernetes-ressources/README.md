# Kubernetes

## 1- Namespace

```bash
kubectl get namespace
kubectl get ns
kubectl create namespace clement
kubectl --namespace clement get all
kubectl -n clement get all
```

## 2- Run a pod

```bash
# Create the pod
kubectl -n clement apply -f nginx.pod.yml
# List pods
kubectl -n get pod
kubectl -n get pod -o wide
kubectl -n get pod -l app=nginx
# Describe pods
kubectl -n describe pod nginx
# Delete a pod with a name
kubectl -n delete pod nginx
# Delete a pod with a manifest
kubectl -n delete -f nginx.pod.yml
```

## 3- Create a deployment

```bash
# Create the deployment
kubectl -n clement apply -f nginx.deploy.yml
# List deployments
kubectl -n clement get deploy
kubectl -n clement get all
# Delete a pod
kubectl -n clement delete pod nginx-deployment-XXXXXXXXXX-XXXXX
kubectl -n clement delete pod -l app=nginx
# Modify the number of replicas in the manifest, then apply it
kubectl -n clement apply -f nginx.deploy.yml
```

## 4- Create a dameonset

```bash
# Create the daemonset
kubectl -n clement apply -f nginx.daemonset.yml
# List deployments
kubectl -n clement get daemonset
kubectl -n clement get all
# Delete a pod
kubectl -n clement delete pod nginx-daemonset-XXXXX
kubectl -n clement delete pod -l app=nginx
```

## 5- Create a job

```bash
# Create the job
kubectl -n clement apply -f sleep.job.yml
# List jobs
kubectl -n clement get job
# Add parallelism `parallelism: 2` and recreate the job
kubectl -n clement delete -f sleep.job.yml
kubectl -n clement apply -f sleep.job.yml
```
