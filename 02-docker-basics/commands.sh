#!/bin/bash

docker pull nginx
docker pull nginx:latest
docker pull nginx:1.27.2
docker images

docker run nginx
docker ps
docker ps -a
docker rm container_name

docker run -d --name server nginx
docker ps
docker logs -f server

docker exec -it server /bin/bash
docker exec -it server htop

docker stop server
docker start server
docker restart server
docker rm server
