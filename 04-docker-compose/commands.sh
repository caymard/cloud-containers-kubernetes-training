#!/bin/bash

# Build image of Python application
docker compose build

# Pull image of Redis server
docker compose pull

# Start containers
docker compose up -d

# Show containers status
docker compose ps

#Attach to containers logs
docker compose logs -f
