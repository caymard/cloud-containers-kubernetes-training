import json
import socket

from fastapi import FastAPI

app = FastAPI()


"""
/
Diplays a Hello World message.
"""
@app.get("/")
def read_root():
    return {"Hello": "World"}

"""
/ip
Returns the hostname and IP of the server.
"""
@app.get("/ip")
def get_ip():
    hostname = socket.gethostname()
    ipaddr = socket.gethostbyname(hostname)
    return {"hostname": hostname, "ip_address": ipaddr}
