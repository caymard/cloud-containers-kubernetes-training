import json
import socket

from fastapi import FastAPI
import redis

app = FastAPI()


"""
Increase counter of visits on Redis database.
"""
def increase_visits_count():
    redis.Redis(host="redis", port=6379, db=0).incr("visits_count")

"""
Returns the number of visits on Redis database.
"""
def get_visits_count():
    return redis.Redis(host="redis", port=6379, db=0).get("visits_count")

"""
/
Diplays a Hello World message.
"""
@app.get("/")
def read_root():
    increase_visits_count()
    return {"Hello": "World"}

"""
/ip
Returns the hostname and IP of the server.
"""
@app.get("/ip")
def get_ip():
    increase_visits_count()
    hostname = socket.gethostname()
    ipaddr = socket.gethostbyname(hostname)
    return {"hostname": hostname, "ip_address": ipaddr}

"""
/count
Returns the counter of visits of the API.
"""
@app.get("/count")
def get_counter():
    increase_visits_count()
    return {"visits_count": get_visits_count()}

"""
/data
Display data of the JSON file.
"""
@app.get("/data")
def get_data():
    increase_visits_count()
    with open("/data/data.json", "r") as f:
        data = json.load(f)
    return data
