#!/bin/bash

docker build -t nginx-plus:latest .


## NETWORKS

docker run -d --name server -p 8008:80 nginx-plus:latest

docker network create networkA
docker network create networkB
docker network ls

docker run -d --name serverA --network networkA nginx-plus:latest
docker run -d --name serverB --network networkA nginx-plus:latest
docker network connect networkB serverB

docker exec -it serverA ip -br a
docker exec -it serverB ip -br a

docker exec -it server ip -br a

docker stop serverA serverB server
docker rm serverA serverB server


## VOLUMES

docker run -d --name server \
    -p 8008:80 \
    -v $PWD/index.html:/usr/share/nginx/html/index.html:ro \
    nginx:latest

docker run -d --name database \
    -p 5432:5432 \
    -e POSTGRES_PASSWORD=training \
    -v db_data:/var/lib/postgresql/data \
    postgres:latest

export PGPASSWORD=training

psql -h localhost -U postgres -c "CREATE TABLE training (id INT PRIMARY KEY NOT NULL);"

docker stop database && docker rm database

psql -h localhost -U postgres -c "\dt"
