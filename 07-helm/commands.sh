# Add Bitnami repository
helm repo add bitnami https://charts.bitnami.com/bitnami

# Install Wordpress
helm install my-blog bitnami/wordpress


# Configuration
# https://github.com/bitnami/charts/tree/main/bitnami/wordpress/#installing-the-chart
helm install wordpress bitnami/wordpress -f values.yml

# Update
helm upgrade wordpress bitnami/wordpress -f values.yml
