#!/bin/bash

terraform plan -destroy -out=clement.tfplan -var-file=clement.tfvars

read -p "Press enter to continue. Press Ctrl+C to abort."

terraform apply clement.tfplan
