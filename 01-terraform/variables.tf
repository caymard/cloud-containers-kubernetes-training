variable "access_key" {
  type        = string
  description = "Access key for Scaleway API."
}

variable "secret_key" {
  type        = string
  description = "Secret key for Scaleway API."
}

variable "project_id" {
  type        = string
  description = "ID of the Scaleway project."
}

variable "ssh_public_ip" {
  type        = string
  description = "IP allowed to access to SSH port of instance."
}

variable "ssh_public_key_path" {
  type        = string
  description = "Path of the public SSH key."
}
