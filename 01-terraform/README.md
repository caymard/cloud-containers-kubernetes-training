# Create ressources with Terraform

## Presentation
This shows how to create 4 Scaleway ressources with Terraform:
- a public IP
- an account SSH key
- a security group
- an instance


## Usage
Define required variables by using a `.tfvars` file or `TF_VAR_` prefixed environment variables.

To create the ressources:
```bash
./create.sh
```

To destroy ressources:
```bash
./destroy.sh
```
