terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
  required_version = ">= 0.13"
}

provider "scaleway" {
  zone       = "pl-waw-2"
  region     = "pl-waw"
  access_key = var.access_key
  secret_key = var.secret_key
}

data "http" "myip" {
  url = "https://ipv4.icanhazip.com"
}
