resource "scaleway_instance_ip" "public_ip" {
  project_id = var.project_id
}

resource "scaleway_account_ssh_key" "main" {
  name       = "main"
  public_key = file(var.ssh_public_key_path)
  project_id = var.project_id
}

resource "scaleway_instance_security_group" "www" {
  project_id              = var.project_id
  inbound_default_policy  = "drop"
  outbound_default_policy = "drop"

  inbound_rule {
    action   = "accept"
    port     = "22"
    ip_range = "${chomp(data.http.myip.response_body)}/32"
  }

  inbound_rule {
    action   = "accept"
    port     = "443"
    ip_range = "0.0.0.0/0"
  }
}

resource "scaleway_instance_server" "web" {
  project_id = var.project_id
  type       = "STARDUST1-S"
  image      = "ubuntu_noble"

  name = "web"
  tags = ["front", "web"]

  ip_id = scaleway_instance_ip.public_ip.id

  root_volume {
    size_in_gb = 10
  }

  security_group_id = scaleway_instance_security_group.www.id

  user_data = {
    cloud-init = file("${path.module}/user-data.sh")
  }
}

resource "scaleway_instance_server" "worker" {
  count      = 2
  project_id = var.project_id
  type       = "STARDUST1-S"
  image      = "ubuntu_noble"

  name = "web-${count.index + 1}"
  tags = ["front", "worker"]

  root_volume {
    size_in_gb = 10
  }

  user_data = {
    cloud-init = file("${path.module}/user-data.sh")
  }
}
