variable "access_key" {
  type        = string
  description = "Access key for Scaleway API."
}

variable "secret_key" {
  type        = string
  description = "Secret key for Scaleway API."
}

variable "project_id" {
  type        = string
  description = "ID of the Scaleway project."
}
