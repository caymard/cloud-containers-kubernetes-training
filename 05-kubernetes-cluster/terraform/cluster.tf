resource "scaleway_k8s_cluster" "training" {
  name       = "training"
  version    = "1.24.3"
  cni        = "cilium"
  project_id = var.project_id
}

resource "scaleway_k8s_pool" "worker" {
  cluster_id = scaleway_k8s_cluster.training.id
  name       = "worker"
  node_type  = "DEV1-M"
  size       = 1
}

resource "local_file" "kubeconfig" {
  content  = scaleway_k8s_cluster.training.kubeconfig[0].config_file
  filename = "${path.module}/training.scw.config"
}
